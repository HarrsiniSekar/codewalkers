/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.ProductCatalogue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kasai
 */
public class GateWay {

    private ProductCatalogue prodCatalogue;

    private ArrayList<Order> orderCat;

    public GateWay() {

        prodCatalogue = new ProductCatalogue();
        orderCat = new ArrayList<Order>();
    }

    public static void main(String args[]) throws IOException {
        GateWay gateway = new GateWay();
        ProductCatalogue prodCatalogue = new ProductCatalogue();
        ArrayList<Order> orderCat = new ArrayList<Order>();
        DataGenerator generator = DataGenerator.getInstance();
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        while ((orderRow = orderReader.getNextRow()) != null) {
            Item item = new Item(Integer.parseInt(orderRow[2]), Integer.parseInt(orderRow[6]), Integer.parseInt(orderRow[3]));
            Order order = new Order(Integer.parseInt(orderRow[0]), Integer.parseInt(orderRow[4]), Integer.parseInt(orderRow[5]), item);
            orderCat.add(order);
        }
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;

        while ((prodRow = productReader.getNextRow()) != null) {
            prodCatalogue.addProduct(Integer.parseInt(prodRow[0]), Integer.parseInt(prodRow[1]),
                    Integer.parseInt(prodRow[2]), Integer.parseInt(prodRow[3]));
        }
        gateway.readData(prodCatalogue, orderCat);
    }

    private void readData(ProductCatalogue p, ArrayList<Order> o) {
        this.prodCatalogue = p;
        this.orderCat = o;


       this.topthreebestNegotiatedProducts();
       this.topthreebestcustomers();
       this.topthreebestSalesPeople();
       this.TotalRevenue();
       

    
        
        
    }

    public void topthreebestNegotiatedProducts() {

        HashMap<Integer, Integer> product = new HashMap<Integer, Integer>();

        for (Product p : prodCatalogue.getProductList()) {

            for (Order o : orderCat) {
                if (p.getProductId() == o.getItem().getProductId()) {

                    if (product.containsKey(p.getProductId())) {
                        product.put(p.getProductId(), product.get(p.getProductId()) + o.getItem().getQuantity());
                    } else {
                        product.put(p.getProductId(), o.getItem().getQuantity());
                    }

                }
            }

        }

        LinkedHashMap<Integer, Integer> mapped = sortHashMapByValues(product, "DESC");
        int count = 0;
        for (int i : mapped.keySet()) {
            if (count < 3) {
                //int prod = this.prodCatalogue.getProductList().get(i).getProductId();
                System.out.println("Prod : " + i + " Value :" + product.get(i));
                count++;
            }
        }
    }

 
  public void topthreebestSalesPeople(){
     HashMap<Integer, Integer> sale = new HashMap<Integer, Integer>();
     
     for (Product p : prodCatalogue.getProductList()) {
         for (Order o : orderCat) {
             if (p.getProductId() == o.getItem().getProductId()){
                 if (sale.containsKey(o.getSupplierId())){
                     sale.put(o.getSupplierId(),sale.get(o.getSupplierId())+((o.getItem().getSalesPrice() - p.getTargetPrice())*o.getItem().getQuantity()));
                 }else {
                     sale.put(o.getSupplierId(), ((o.getItem().getSalesPrice() - p.getTargetPrice())*o.getItem().getQuantity()));
                 }
         }
     }}
             LinkedHashMap<Integer, Integer> mapped = sortHashMapByValues(sale, "DESC");
        int count = 0;
        System.out.println("\n3).Three Sales people who have the most profit \n");
        for (int i : mapped.keySet()) {           
            if (count < 3) {
                System.out.println("SupplierId : " + i + " Value :" + sale.get(i));
                count++;
                if(sale.get(i)==sale.get(i+1)){
                    count--;
                }
            }
        }
 }
   public void TotalRevenue(){
     HashMap<Integer, Integer> totalRevenue = new HashMap<Integer, Integer>();
     
     for (Product p : prodCatalogue.getProductList()) {
         for (Order o : orderCat) {
             if (p.getProductId() == o.getItem().getProductId()){
                 if (totalRevenue.containsKey(o.getSupplierId())){
                     totalRevenue.put(o.getSupplierId(),totalRevenue.get(o.getSupplierId())+((o.getItem().getSalesPrice() - p.getTargetPrice())*o.getItem().getQuantity()));
                 }else {
                     totalRevenue.put(o.getSupplierId(), ((o.getItem().getSalesPrice() - p.getTargetPrice())*o.getItem().getQuantity()));
                 }
         }
     }}
             LinkedHashMap<Integer, Integer> mapped = sortHashMapByValues(totalRevenue, "DESC");
        int count = 0;
        int sum = 0;
        System.out.println("\n4).Total Revenue \n"); 
        for (int i : mapped.keySet()) {  
            
            if (count < mapped.size()) {
                sum += totalRevenue.get(i);
                count++;
            }      
        }
        System.out.println("Total Revenue : " + sum) ;
        //System.out.println("Total Revenue : " + Math.abs(sum)) ;
        
 }

    public void topthreebestcustomers(){
        HashMap<Integer, Integer> customers = new HashMap<Integer, Integer>();

        for (Product p : prodCatalogue.getProductList()) {

            for (Order o : orderCat) {
                
                if (p.getProductId() == o.getItem().getProductId() ){
                   // int finaly = o.getItem().getSalesPrice() - p.getTargetPrice();
                    if (customers.containsKey(o.getCustomerId())) {
                        customers.put(o.getCustomerId(), customers.get(o.getCustomerId()) + 
                                (Math.abs(o.getItem().getSalesPrice() - p.getTargetPrice())* o.getItem().getQuantity()));
                    } else {
                        customers.put(o.getCustomerId(), (Math.abs(o.getItem().getSalesPrice() - p.getTargetPrice()) * o.getItem().getQuantity()));
                    }

                }
            }

        }
       

        LinkedHashMap<Integer, Integer> mapped = sortHashMapByValues(customers, "ASC");
        int count = 0;
         System.out.println("\n");
        System.out.print("Question 2 - Find the best customers");
        System.out.println("\n");
        for (int i : mapped.keySet()) {
            if (count < 3) {

                System.out.println("Customr_ID: " + i + " Value :" + customers.get(i));
                count++;
            }
        }
    }
    
            
    public LinkedHashMap<Integer, Integer> sortHashMapByValues(
            HashMap<Integer, Integer> passedMap, String order) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        if (order.equalsIgnoreCase("ASC")) {
            Collections.sort(mapValues);
            Collections.sort(mapKeys);
        } else {
            Collections.sort(mapValues, Collections.reverseOrder());
            Collections.sort(mapKeys, Collections.reverseOrder());

        }

        LinkedHashMap<Integer, Integer> sortedMap
                = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            
            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }


}
