/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Product {

    private int productId;

    private int minPrice;

    private int maxPrice;

    private int targetPrice;

    Product(int productId, int min_Price, int max_Price, int target_Price) {

        this.productId = productId;
        this.minPrice = min_Price;
        this.maxPrice = max_Price;
        this.targetPrice = target_Price;

    }

    public int getProductId() {
        return productId;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }
    

}
