/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;

/**
 *
 * @author Sanjeev MD
 */
public class ProductCatalogue {

    private ArrayList<Product> productList;

    public ProductCatalogue() {
        this.productList = new ArrayList<Product>();
    }
    
    public void addProduct(int productId, int minPrice, int maxPrice, int targetPrice){
        Product product= new Product(productId,minPrice,maxPrice,targetPrice);
        productList.add(product);
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
}
