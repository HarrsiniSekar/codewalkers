/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.AirlineDirectory;
import Business.Customer.Customer;
import Business.Customer.CustomerDirectory;
import Business.Flight.FlightSchedule;
import Business.Seat.Category;
import Business.Seat.SeatClass;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Sanjeev MD
 */
public class SeatBooking extends javax.swing.JPanel {

    private JPanel userProcess;

    private FlightSchedule flightSchedule;

    private Object category;

    private int passengersNumber;

    private static int i = 1;

    private ArrayList<Customer> cust;

    private AirlineDirectory airlinedir;

    private CustomerDirectory customer;

    /**
     * Creates new form SeatBooking
     */
    SeatBooking(JPanel userProcess, FlightSchedule schedule, Object category, String passengersNumber, AirlineDirectory airlinedir,
            CustomerDirectory customer) {
        initComponents();
        this.userProcess = userProcess;
        this.flightSchedule = schedule;
        this.category = category;
        this.airlinedir = airlinedir;
        this.customer = customer;
        this.passengersNumber = Integer.parseInt(passengersNumber);
        populateTableComboBox();
        this.cust = new ArrayList<Customer>();
    }

    public void populateTableComboBox() {
        Category cat = null;
        if (category == Category.Economy.toString()) {
            cat = Category.Economy;
        }
        if (category == Category.Business.toString()) {
            cat = Category.Business;
        }
        if (category == Category.FirstClass.toString()) {
            cat = Category.FirstClass;
        }
        HashSet<SeatClass> seats = new HashSet<SeatClass>();
        seats = this.flightSchedule.getClassSeats(cat);

        HashSet<Integer> row = new HashSet<>();
        HashSet<Integer> column = new HashSet<>();
        for (SeatClass seat : seats) {
            row.add(seat.getRow());
            column.add(seat.getColumn());
        }

        for (Integer i : row) {
            jComboBox2.addItem(i + "");
        }
        for (Integer i : column) {
            if (i == 1) {
                jComboBox3.addItem(i + "- Left Window");
            }
            if (i == 2) {
                jComboBox3.addItem(i + " - Left Middle");
            }
            if (i == 3) {
                jComboBox3.addItem(i + " - Left Aisle");
            }
            if (i == 4) {
                jComboBox3.addItem(i + " - Right Aisle");
            }
            if (i == 5) {
                jComboBox3.addItem(i + " - Right Middle");
            }
            if (i == 6) {
                jComboBox3.addItem(i + " - Right Window");
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox2 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Row" }));

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Column" }));

        jButton1.setText("Book ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Customer Name");

        jLabel2.setText("Passenger Details");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(44, 44, 44)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(326, 326, 326)
                        .addComponent(jLabel2)))
                .addContainerGap(429, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel2)
                .addGap(84, 84, 84)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(36, 36, 36)
                .addComponent(jButton1)
                .addContainerGap(198, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        if (jComboBox2.getSelectedItem() == "Row" || jComboBox3.getSelectedItem() == "Column") {
            JOptionPane.showMessageDialog(userProcess, "Please Select Row and column of the seat");
            return;
        }
        if (jTextField1.getText().isEmpty()) {
            JOptionPane.showMessageDialog(userProcess, " Please enter Customer details ");
            return;
        }
        int Row = Integer.parseInt(jComboBox2.getSelectedItem().toString().substring(0, 1));
        int Column = Integer.parseInt(jComboBox3.getSelectedItem().toString().substring(0, 1));

        if (!flightSchedule.getAvail(Row, Column)) {
            JOptionPane.showMessageDialog(userProcess, "Sorry seat is under process, Book another seat");
            return;
        }
        Customer customer = new Customer();
        customer.setName(jTextField1.getText());
        cust.add(customer);
        flightSchedule.proces(Column, Row, customer);
        if (i == passengersNumber) {
            flightSchedule.bookProcessedSeats();;
            for (Customer c : cust) {
                c.setID(this.customer.setcust(c));
            }
            JOptionPane.showMessageDialog(userProcess, "Seats are booked");
            BookingConfirmation book = new BookingConfirmation(airlinedir, this.customer, cust);
            userProcess.add("Menu", book);
            CardLayout layout = (CardLayout) userProcess.getLayout();
            layout.next(userProcess);
        }
        if (i < passengersNumber) {
            i++;
        }
        jTextField1.setText("");
        populateTableComboBox();

        //flightSchedule.bookaSeat(Column, Row, customer);

    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
