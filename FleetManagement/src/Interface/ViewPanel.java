/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Airline;
import Business.AirlineDirectory;
import Business.Flight.Flight;
import Business.Flight.FlightSchedule;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashSet;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author harshi
 */
public class ViewPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewPanel
     */
    private JPanel userProcess;
    private FlightSchedule flightschedule;
    private Airline airline;
    private AirlineDirectory airlinelist;
  
    ViewPanel(JPanel userProcess, FlightSchedule flightschedule, Object search,AirlineDirectory airlinelist) {
       initComponents();
       this.flightschedule = flightschedule;
       this.userProcess = userProcess;
       for(Airline a : airlinelist.getAirLineList()){
          if(a.getAirlineName().contains(search.toString())){
                   this.airline = a;
    }
       }
       this.airlinelist = airlinelist;
       flight();
        //combo();
        UpdateDetails();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        DepHrsComboBox = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        SchedulenumberTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        SourceTextField = new javax.swing.JTextField();
        DepMinCombobox = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        ArrHrsCombobox = new javax.swing.JComboBox<>();
        DestinationTextField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        ArrMinComboBox = new javax.swing.JComboBox<>();
        btnUpdate = new javax.swing.JButton();
        FlightNameTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnConfirm = new javax.swing.JButton();
        DateTextfield = new javax.swing.JTextField();
        modelNumberTextField = new javax.swing.JTextField();

        jLabel7.setText("Date:");

        DepHrsComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", " " }));
        DepHrsComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepHrsComboBoxActionPerformed(evt);
            }
        });

        jLabel8.setText("Schedule Number:");

        jLabel11.setText("Flight Number:");

        jLabel2.setText("Airline Name:");

        DepMinCombobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", " " }));

        jLabel4.setText("Destination :");

        ArrHrsCombobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", " " }));
        ArrHrsCombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ArrHrsComboboxActionPerformed(evt);
            }
        });

        jLabel3.setText("Source:");

        jLabel5.setText("Departure time:");

        ArrMinComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", " " }));

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        jLabel6.setText("Arrival Time:");

        jLabel1.setFont(new java.awt.Font("Yuanti TC", 0, 24)); // NOI18N
        jLabel1.setText("Flight Schedule");

        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        modelNumberTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modelNumberTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(220, 220, 220))
            .addGroup(layout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addComponent(jLabel2)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(SourceTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                            .addComponent(DestinationTextField)
                            .addComponent(FlightNameTextField))
                        .addGap(120, 120, 120))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnUpdate)
                            .addComponent(DateTextfield, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(106, 106, 106)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(18, 18, 18)
                                .addComponent(modelNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(ArrHrsCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel5)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(DepHrsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLabel6))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ArrMinComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(DepMinCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addGap(18, 18, 18)
                            .addComponent(SchedulenumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(43, 43, 43)))
                    .addComponent(btnConfirm))
                .addContainerGap(78, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(DepHrsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DepMinCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FlightNameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(SourceTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ArrHrsCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ArrMinComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(DestinationTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(modelNumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SchedulenumberTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DateTextfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate)
                    .addComponent(btnConfirm))
                .addGap(258, 258, 258))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void flight()
    {
    FlightNameTextField.setText(this.airline.getAirlineName());
    FlightNameTextField.setEnabled(false);
    modelNumberTextField.setEnabled(false);
    } 
    
      private void UpdateDetails(){
        
        for(Airline a : airlinelist.getAirLineList()){
            
            for(Flight f : a.getFleets()){
                f.getFlightNumber().equals(modelNumberTextField.getText());
                            for(FlightSchedule fl : f.getAirplane()){
                                SourceTextField.setText(fl.getFrom());
                                DestinationTextField.setText(fl.getTo());
                                SchedulenumberTextField.setText(fl.getSchedulenumber());
                                String DepTime = (String) fl.getDepartureTime();
                                String Hour=DepTime.split(":")[0];
                                String Minute=DepTime.split(":")[1];
                                String dephr=String.valueOf(Hour);
                                String depmin=String.valueOf(Minute);
                                DepHrsComboBox.setSelectedItem(dephr);
                                DepMinCombobox.setSelectedItem(depmin);
        
                                modelNumberTextField.setText(f.getFlightNumber());
                                String ArrTime = (String) fl.getArrivalTime();
                                String ArrHour=ArrTime.split(":")[0];
                                String ArrMinute=ArrTime.split(":")[1];
                                String arrhr =String.valueOf(ArrHour);
                                String arrmin =String.valueOf(ArrMinute);
                                ArrHrsCombobox.setSelectedItem(arrhr);
                                ArrMinComboBox.setSelectedItem(arrmin);
        
                                DateTextfield.setText(String.valueOf(fl.getDepartureDate()));
                            }
                        }
                    }
      
                
            
          
          
        

        ArrHrsCombobox.setEnabled(false);
        ArrMinComboBox.setEnabled(false);
        DepHrsComboBox.setEnabled(false);
        DepMinCombobox.setEnabled(false);
        DestinationTextField.setEnabled(false);
        SourceTextField.setEnabled(false);
        DateTextfield.setEnabled(false);
        SchedulenumberTextField.setEnabled(false);
        modelNumberTextField.setEnabled(false);
        btnConfirm.setEnabled(false);
        btnUpdate.setEnabled(true);
        
    }
    
//      private void combo(){
//        
//        HashSet<String> hash = new HashSet<>();
//        for (Flight u : airline.getFleets()) {
//            if(u.getFlightNumber()!= null) 
//            {
//                hash.add(u.getFlightNumber());
//            }
//        }
//        
//        for(String u: hash){
//                FlightNumberComboBox.addItem(u);            
//        }      
//        
//    }
    private void DepHrsComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepHrsComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DepHrsComboBoxActionPerformed

    private void ArrHrsComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ArrHrsComboboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ArrHrsComboboxActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        ArrHrsCombobox.setEnabled(true);
        ArrMinComboBox.setEnabled(true);
        DepHrsComboBox.setEnabled(true);
        DepMinCombobox.setEnabled(true);
        DestinationTextField.setEnabled(true);
        SourceTextField.setEnabled(true);
        DateTextfield.setEnabled(false);
        SchedulenumberTextField.setEnabled(true);
        modelNumberTextField.setEnabled(false);
        btnConfirm.setEnabled(true);

    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        // TODO add your handling code here:
     for(Airline a : airlinelist.getAirLineList()){
            
            for(Flight f : a.getFleets()){
                modelNumberTextField.setText(f.getFlightNumber());
                            for(FlightSchedule fl : f.getAirplane()){    
        
                                    String source = SourceTextField.getText();
                                    if( source == null || source.isEmpty())
                                      {
                                    JOptionPane.showMessageDialog(null,"Source cannot be empty");
                                    return;
                                      }

                                    String destination = DestinationTextField.getText();
                                    if( destination == null || destination.isEmpty()){
                                    JOptionPane.showMessageDialog(null,"Destination cannot be empty");
                                    return;
                                      }

                            
   // DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    // tring date_to_store = sysDate.format(d1.getDate()).toString();
   
        String selectedDate = DateTextfield.getText();
                                    
        String dephrs = String.valueOf(DepHrsComboBox.getSelectedItem());
        String depmins = String.valueOf(DepMinCombobox.getSelectedItem());
        String arrhrs = String.valueOf(ArrHrsCombobox.getSelectedItem());
        String arrmins = String.valueOf(ArrMinComboBox.getSelectedItem());
        
        String deptime = dephrs + ":" + depmins;
        String arrtime = arrhrs + ":" + arrmins;

        LocalTime t1 = LocalTime.parse(deptime);
        LocalTime t2 = LocalTime.parse(arrtime);
        Duration diff = Duration.between(t2, t1);
        String totalhours = String.valueOf(diff.toHours());
        
        String schedulenum = SchedulenumberTextField.getText();
        if( schedulenum == null || schedulenum.isEmpty())
        {
            JOptionPane.showMessageDialog(null,"Schedule number cannot be empty");
            return;
        }
        if(fl.getSchedulenumber().equals(schedulenum)){
          JOptionPane.showMessageDialog(null, "Schedule number already exists, please enter the unique schedule number"); 
          return;
        }
        
                JOptionPane.showMessageDialog(null, "Flight Schedule updated successfully"); 
                
         
        fl.setFrom(source);
        fl.setDepartureTime(deptime);
        fl.setArrivalTime(arrtime);
        fl.setTo(destination);
        fl.setSchedulenumber(schedulenum);
        fl.setHours(totalhours);
        fl.setDepartureDate(selectedDate);
                       return;

     }
  }
            
            
}

        ArrHrsCombobox.setEnabled(false);
        ArrMinComboBox.setEnabled(false);
        DepHrsComboBox.setEnabled(false);
        DepMinCombobox.setEnabled(false);
        DestinationTextField.setEnabled(false);
        SourceTextField.setEnabled(false);
        DateTextfield.setEnabled(false);
        SchedulenumberTextField.setEnabled(false);
        modelNumberTextField.setEnabled(false);
        btnConfirm.setEnabled(false);
        btnUpdate.setEnabled(false);
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void modelNumberTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modelNumberTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_modelNumberTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> ArrHrsCombobox;
    private javax.swing.JComboBox<String> ArrMinComboBox;
    private javax.swing.JTextField DateTextfield;
    private javax.swing.JComboBox<String> DepHrsComboBox;
    private javax.swing.JComboBox<String> DepMinCombobox;
    private javax.swing.JTextField DestinationTextField;
    private javax.swing.JTextField FlightNameTextField;
    private javax.swing.JTextField SchedulenumberTextField;
    private javax.swing.JTextField SourceTextField;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField modelNumberTextField;
    // End of variables declaration//GEN-END:variables
}
