/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Airline;
import Business.AirlineDirectory;
import Business.Customer.Customer;
import Business.Customer.CustomerDirectory;
import Business.Flight.Flight;
import Business.Flight.FlightSchedule;
import Business.Seat.SeatClass;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sanjeev MD
 */
public class BookingConfirmation extends javax.swing.JPanel {

    private AirlineDirectory airlinedir;

    private CustomerDirectory cust;

    private ArrayList<Customer> sessCust;

    /**
     * Creates new form BookingConfirmation
     */
    public BookingConfirmation(AirlineDirectory airdir, CustomerDirectory cust, ArrayList<Customer> sessCust) {
        initComponents();
        this.airlinedir = airdir;
        this.cust = cust;
        this.sessCust = sessCust;
        populateTable();
    }

    public void populateTable() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        dtm.setRowCount(0);
        for (Airline a : airlinedir.getAirLineList()) {
            for (Flight f : a.getFleets()) {
                for (FlightSchedule fs : f.getAirplane()) {
                    for (SeatClass s : fs.getSeats()) {
                        if (s.isIsbooked()) {
                            for (Customer c : sessCust) {
                                if (s.getCustomer() != null && s.getCustomer().getID() == c.getID()) {
                                    Object row[] = new Object[9];   
                                    row[0] = c.getName();
                                    row[1] = s.getRow();
                                    row[2] = s.getColumn();
                                    row[3] = fs.getFrom();
                                    row[4] = fs.getTo();
                                    row[5] = fs.getDepartureTime() == null ? "" : fs.getDepartureTime();
                                    row[6] = fs.getArrivalTime() == null ? "" : fs.getArrivalTime();
                                    row[7] = a.getAirlineName() == null ? "" : a.getAirlineName();
                                    row[8] = fs.getSchedulenumber() == null ? "" : fs.getSchedulenumber();
                                    dtm.addRow(row);
                                }
                            }
                        }

                    }
                }
            }

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Customer", "Seat row", "Seat Column", "from ", "To", "Departure Time", "ArrivalTime", "Airline", "Flight Number"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("Booking Confirmation");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(324, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(338, 338, 338))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
