/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Seat;

import Business.Customer.Customer;

/**
 *
 * @author Sanjeev MD
 */
public class SeatClass {

   
    private int ID;

    private Category Category;

    private Position Postition;

    private Customer Customer;

    private boolean isbooked;

    private int Price;

    private int Row;

    private int column;
    
    private boolean underProcess;

    public boolean isUnderProcess() {
        return underProcess;
    }

    public void setUnderProcess(boolean underProcess) {
        this.underProcess = underProcess;
    }

    public int getRow() {
        return Row;
    }

    public void setRow(int Row) {
        this.Row = Row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public boolean isIsbooked() {
        return isbooked;
    }

    public void setIsbooked(boolean isbooked) {
        this.isbooked = isbooked;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Category getCategory() {
        return Category;
    }

    public void setCategory(Category Category) {
        this.Category = Category;
    }

    public Position getPostition() {
        return Postition;
    }

    public void setPostition(Position Postition) {
        this.Postition = Postition;
    }

    public Customer getCustomer() {
        return Customer;
    }

    public void setCustomer(Customer Customer) {
        this.Customer = Customer;
    }

    private String getSeatNumber() {
        return this.Postition.name().substring(0, 2) + this.Category.name().substring(0, 2) + ID;
    }

}
