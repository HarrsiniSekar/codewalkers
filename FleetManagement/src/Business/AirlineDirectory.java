/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Flight.Flight;
import Business.Flight.FlightSchedule;
import java.util.ArrayList;

/**
 *
 * @author Sanjeev MD
 */
public class AirlineDirectory {

    private ArrayList<Airline> airlineList;

    public AirlineDirectory() {
        airlineList = new ArrayList<Airline>();
        ArrayList<Flight> fleets1 = new ArrayList<Flight>();
        ArrayList<Flight> fleets2 = new ArrayList<Flight>();
        ArrayList<Flight> fleets3 = new ArrayList<Flight>();
        ArrayList<Flight> fleets4 = new ArrayList<Flight>();
        ArrayList<Flight> fleets5 = new ArrayList<Flight>();
        ArrayList<Flight> fleets6 = new ArrayList<Flight>();
        ArrayList<Flight> fleets7 = new ArrayList<Flight>();
        ArrayList<Flight> fleets8 = new ArrayList<Flight>();
        ArrayList<Flight> fleets9 = new ArrayList<Flight>();
        ArrayList<Flight> fleets10 = new ArrayList<Flight>();
        
        ArrayList<FlightSchedule> flightschedule1 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule2 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule3 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule4 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule5 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule6 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule7 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule8 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule9 = new ArrayList<FlightSchedule>();
        ArrayList<FlightSchedule> flightschedule10 = new ArrayList<FlightSchedule>();
        
        FlightSchedule schedule1 = new FlightSchedule("Chennai", "Boston", "05:00", "22:00","S123","2019-10-19","17");
        FlightSchedule schedule2 = new FlightSchedule("Boston", "Doha", "14:00", "22:00","S124","2019-10-20","08");
        FlightSchedule schedule3 = new FlightSchedule("London", "Dallas", "12:00", "23:00","S125","2019-10-02","11");
        FlightSchedule schedule4 = new FlightSchedule("Bangalore", "Boston", "14:00", "21:00","S189","2019-10-09","07");
        FlightSchedule schedule5 = new FlightSchedule("Delhi", "Sydney", "06:00", "18:00","S098","2019-10-17","12");
        FlightSchedule schedule6 = new FlightSchedule("Bangalore", "Singapore", "09:00", "17:00","S090","2019-10-23","08");
        FlightSchedule schedule7 = new FlightSchedule("Goa", "Boston", "17:00", "23:00","A123","2019-09-30","06");
        FlightSchedule schedule8 = new FlightSchedule("Dallas", "Boston", "11:00", "23:00","A456","2019-09-23","12");
        FlightSchedule schedule9 = new FlightSchedule("Frankfurt", "Boston", "10:00", "22:00","Z123","2019-08-08","12");
        FlightSchedule schedule10 = new FlightSchedule("London", "Sydney", "05:00", "21:00","R456","2019-09-21","16");
        FlightSchedule schedule11 = new FlightSchedule("Chennai", "Boston", "05:00", "21:00","R457","2019-09-21","16");
        
        flightschedule1.add(schedule1);
        flightschedule2.add(schedule2);
        flightschedule3.add(schedule3);
        flightschedule4.add(schedule4);
        flightschedule5.add(schedule5);
        flightschedule6.add(schedule6);
        flightschedule7.add(schedule7);
        flightschedule8.add(schedule8);
        flightschedule9.add(schedule9);
        flightschedule10.add(schedule10);
        flightschedule4.add(schedule11);
        
        
        Flight flight1 = new Flight("Airbus", "A300", "Active", true, true,flightschedule1);
        Flight flight2 = new Flight("Airbus", "A310", "Active", true, true,flightschedule2);
        Flight flight3 = new Flight("Airbus", "A318", "Active", true, true,flightschedule3);
        Flight flight4 = new Flight("Airbus", "A319", "Active", true, true,flightschedule4);
        Flight flight5 = new Flight("Airbus", "A320", "Active", true, true,flightschedule5);
        Flight flight6 = new Flight("Airbus", "A321", "Active", true, true,flightschedule6);
        Flight flight7 = new Flight("Airbus", "A330", "Active", true, true,flightschedule7);
        Flight flight8 = new Flight("Airbus", "A350", "Active", true, true,flightschedule8);
        Flight flight9 = new Flight("Airbus", "A380", "Active", true, true,flightschedule9);
        Flight flight10 = new Flight("Beoing", "B717", "Active", true, true,flightschedule10);
//        Flight flight11 = new Flight("Beoing", "B727", "Active", true, true,flightschedule10);
//        Flight flight12 = new Flight("Beoing", "B737", "Active", true, true,flightschedule9);
//        Flight flight13 = new Flight("Beoing", "B747", "Active", true, true,flightschedule8);
//        Flight flight14 = new Flight("Beoing", "B757", "Active", true, true,flightschedule7);
//        Flight flight15 = new Flight("Beoing", "B767", "Active", true, true,flightschedule6);
//        Flight flight16 = new Flight("Beoing", "B777", "Active", true, true,flightschedule5);
//        Flight flight17 = new Flight("Beoing", "B787", "Active", true, true,flightschedule4);
//        Flight flight18 = new Flight("Beoing", "B787 Dreamliner", "Active", true, true,flightschedule3);

        fleets1.add(flight1);
        fleets2.add(flight2);
        fleets3.add(flight3);
        fleets4.add(flight4);
        fleets5.add(flight5);
        fleets6.add(flight6);
        fleets7.add(flight7);
        fleets8.add(flight8);
        fleets9.add(flight9);
        fleets10.add(flight10);
//        fleets3.add(flight3);
//        fleets4.add(flight4);
//        fleets5.add(flight5);
//        fleets6.add(flight6);
//        fleets7.add(flight7);
//        fleets8.add(flight8);
//        fleets9.add(flight9);
//        fleets10.add(flight10);
//        fleets5.add(flight11);
//        fleets6.add(flight12);
//        fleets7.add(flight13);
//        fleets8.add(flight14);
//        fleets9.add(flight15);


        
        Airline airline1 = new Airline("Qatar","Doha","100M","2B","Going Places Together",fleets1);
        Airline airline2 = new Airline("AirAsia","Malaysia","180M","4B","Now everyone can fly", fleets2);
        Airline airline3 = new Airline("Emirates","Dubai","120M","3.5B","Fly Emirates,Fly better",fleets3);
        Airline airline4 = new Airline("Skyjet","Delhi","10M","150M","A better way to fly",fleets4);
        Airline airline5 = new Airline("American Airlines","New York","900M","6B","Great is what we are going for",fleets5);
        Airline airline6 = new Airline("Air India","Delhi","50M","300M","Truly Indian",fleets6);
        Airline airline7 = new Airline("Malaysian Airlines","Malaysia","250M","3.8B","Malaysian hospitality",fleets7);
        Airline airline8 = new Airline("Singapore Airlines","Singapore","200M","2.4B","A great way to fly",fleets8);
        Airline airline9 = new Airline("Lufthansa","Frankfurt","800M","5.1M","Say yes to the world",fleets9);
        Airline airline10 = new Airline("British Airways","Lodon","550M","2.2B","To Fly.To Serve",fleets10);
   
        airlineList.add(airline1);
        airlineList.add(airline2);
        airlineList.add(airline3);
        airlineList.add(airline4);
        airlineList.add(airline5);
        airlineList.add(airline6);
        airlineList.add(airline7);
        airlineList.add(airline9);
        airlineList.add(airline10);          
    }

    public void addAirLine(Airline airline) {
        airlineList.add(airline);
    }

    public ArrayList<Airline> getAirLineList() {
        return this.airlineList;
    }
    
    public void setAirlineList(ArrayList<Airline> airlineList) {
        this.airlineList = airlineList;
    }

    public int getFleetCount() {
        return airlineList.size();
    }
    public void deleteAirline(Airline airline){
        airlineList.remove(airline);
    }
    
    public ArrayList<FlightSchedule> getallschedule(){
        ArrayList<FlightSchedule> fss = new ArrayList<FlightSchedule>();
        for( Airline a : getAirLineList()){
            for(Flight f : a.getFleets()){
                for(FlightSchedule fs: f.getAirplane()){
                                    fss.add(fs);
                }
            
            }
            
        }return fss;
    
    }
}
    
