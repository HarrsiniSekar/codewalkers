/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Flight;

import Business.Customer.Customer;
import Business.Seat.Category;
import Business.Seat.Position;
import Business.Seat.SeatClass;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Sanjeev MD
 */
public class FlightSchedule {

    private String from;

    private String to;

    private String departureTime;

    private String ArrivalTime;

    private String hours;
    
    private String DepartureDate;

    private String Schedulenumber;

    private HashSet<SeatClass> Seats;

    public FlightSchedule(String from, String to, String DepartureTime,
            String ArrivalTime, String Schedulenumber, String departureDate, String hours) {
        this.from = from;
        this.to = to;
        this.departureTime = DepartureTime;
        this.ArrivalTime = ArrivalTime;
        this.Schedulenumber = Schedulenumber;
        this.DepartureDate = departureDate;
        this.hours = hours;
        Seats = new HashSet<SeatClass>();
        int id = 0;
        for (int i = 1; i <= 25; i++) {
            for (int j = 1; j <= 6; j++) {
                SeatClass s = new SeatClass();
                s.setRow(i);
                s.setColumn(j);
                if (j == 1 || j == 6) {
                    s.setPostition(Position.Window);
                }
                if (j == 2 || j == 5) {
                    s.setPostition(Position.Middle);
                }
                if (j == 3 || j == 4) {
                    s.setPostition(Position.Aisle);
                }
                if (i <= 5) {
                    s.setCategory(Category.Business);
                    s.setPrice(500);
                }
                if (i > 5 && i <= 10) {
                    s.setCategory(Category.FirstClass);
                    s.setPrice(400);
                }
                if (i > 10 && i <= 25) {
                    s.setCategory(Category.Economy);
                    s.setPrice(300);
                }
                s.setIsbooked(false);
                s.setID(id);
                id++;
                Seats.add(s);
            }
        }
    
}

    public String getFrom() {
        return from;
    }

    public String getDepartureDate() {
        return DepartureDate;
    }

    public String getSchedulenumber() {
        return Schedulenumber;
    }

    public void setSchedulenumber(String Schedulenumber) {
        this.Schedulenumber = Schedulenumber;
    }

    public void setDepartureDate(String DepartureDate) {
        this.DepartureDate = DepartureDate;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(String ArrivalTime) {
        this.ArrivalTime = ArrivalTime;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    @Override
    public String toString() {
        return this.from;
    }

    public int getClassSeatAvailability(Category category) {
        int actualAvail = 0;
        if (category == Category.Business || category == Category.FirstClass) {
            actualAvail = 30;
        }
        if (category == Category.Economy) {
            actualAvail = 90;
        }
        int avail = 0;

        HashSet<SeatClass> seatslist = new HashSet<SeatClass>();
        for (SeatClass s : Seats) {
            if (s.getCategory().equals(category) && s.isIsbooked() == false) {
                seatslist.add(s);
            }
        }
        return seatslist.size();
    }

    public HashSet<SeatClass> getClassSeats(Category category) {

        HashSet<SeatClass> seatslist = new HashSet<SeatClass>();
        for (SeatClass s : Seats) {
            if (s.getCategory().equals(category) && s.isIsbooked() == false && s.isUnderProcess() == false) {
                seatslist.add(s);
            }
        }
        return seatslist;
    }

    public void bookaSeat(int column, int row, Customer customer) {

        for (SeatClass s : Seats) {
            if (s.getRow() == row && s.getColumn() == column) {
                s.setIsbooked(true);
                s.setCustomer(customer);
            }
        }

    }

    public boolean getAvail(int row, int column) {
        boolean b = false;
        for (SeatClass s : Seats) {
            if (s.getRow() == row && s.getColumn() == column
                    && s.isIsbooked() == false
                    && s.isUnderProcess() == false) {
                b = true;
            }
        }
        return b;
    }
    public HashSet<SeatClass> getSeats(){
        return this.Seats;
    }

    public void bookProcessedSeats() {
        for (SeatClass s : Seats) {
            if (s.isUnderProcess() == true) {
                s.setUnderProcess(false);
                s.setIsbooked(true);
            }
        }

    }

    public void proces(int column, int row, Customer customer) {

        for (SeatClass s : Seats) {
            if (s.getRow() == row && s.getColumn() == column) {
                s.setUnderProcess(true);
                s.setCustomer(customer);
            }
        }

    }

}
