/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Flight;

import Business.Airline;
import Business.AirlineDirectory;
import java.util.ArrayList;

/**
 *
 * @author Sanjeev MD
 */
public class Flight {
    

    private String flightModel;

    private String flightNumber;
    
    private boolean inHouseMedia; 
    
    private boolean wifiAvailability; 
    
    private String flightStatus;
                
    private ArrayList<FlightSchedule> airplane;
    
    private AirlineDirectory airline;

    public Flight (String flightModel, String flightNumber, String flightStatus, boolean inHouseMedia, boolean wifiAvailability
            ,ArrayList<FlightSchedule> airplane){
        this.flightModel=flightModel;
        this.flightNumber=flightNumber;
        this.flightStatus=flightStatus;
        this.inHouseMedia=inHouseMedia;
        this.wifiAvailability=wifiAvailability;
        this.airplane = airplane;
    }
  
    
    @Override
    public String toString(){
       return this.flightNumber;
    }
    public boolean isInHouseMedia() {
        return inHouseMedia;
    }

    public void setInHouseMedia(boolean inHouseMedia) {
        this.inHouseMedia = inHouseMedia;
    }

    public boolean iswifiAvailability() {
        return wifiAvailability;
    }

    public void setwifiAvailability(boolean wifiAvailability) {
        this.wifiAvailability = wifiAvailability;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    
    public ArrayList<FlightSchedule> getAirplane() {
        return this.airplane;
    }

    public void setAirplane(ArrayList<FlightSchedule> airplane) {
        this.airplane = airplane;
    }

    public void addschedule(FlightSchedule fl) 
    {
        this.airplane.add(fl);   
    }
    
     public void cancelschedule(FlightSchedule fl){
        this.airplane.remove(fl);
    }  

    public String getFlightModel() {
        return flightModel;
    }

    public boolean isWifiAvailability() {
        return wifiAvailability;
    }


    public void setFlightModel(String flightModel) {
        this.flightModel = flightModel;
    }

    public String getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(String flightStatus) {
        this.flightStatus = flightStatus;
    }  


    //public void setAirplane(ArrayList<FlightSchedule> airplane) {
        //this.airplane = airplane;
    
      

}
