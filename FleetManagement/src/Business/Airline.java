/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Flight.Flight;
import java.util.ArrayList;

/**
 *
 * @author Sanjeev MD
 */
public class Airline {

    private ArrayList<Flight> fleets;
    private String airlineName;
    private String headQuarters;
    private String netIncome;
    private String revenue;
    private String companySlogan;
    private Flight flight;

    public Airline(String airlineName, String headQuarters, String netIncome, String revenue, String companySlogan, ArrayList<Flight> fleets) {
        this.airlineName = airlineName;
        this.headQuarters = headQuarters;
        this.netIncome = netIncome;
        this.revenue = revenue;
        this.companySlogan = companySlogan;
        this.fleets = fleets;
        
    }
    public void addFleet(Flight flight) {   
        this.fleets.add(flight);   
    }

    @Override
    public String toString() {
        return this.airlineName;
    }

    public ArrayList<Flight> getFleets() {
        return fleets;
    }

    public void setFleets(ArrayList<Flight> fleets) {
        this.fleets = fleets;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getHeadQuarters() {
        return headQuarters;
    }

    public void setHeadQuarters(String headQuarters) {
        this.headQuarters = headQuarters;
    }

    public String getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(String netIncome) {
        this.netIncome = netIncome;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getCompanySlogan() {
        return companySlogan;
    }

    public void setCompanySlogan(String companySlogan) {
        this.companySlogan = companySlogan;
    }

    public int getFleetCount() {
        try {
            return this.fleets.size();
        }catch(NullPointerException e) {
            return 0;
        }
    } 
    public void deleteFlight(Flight flight){
        fleets.remove(flight);
    }
}
   