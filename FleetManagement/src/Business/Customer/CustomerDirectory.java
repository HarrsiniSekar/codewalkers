/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Customer;

import java.util.ArrayList;

/**
 *
 * @author Sanjeev MD
 */
public class CustomerDirectory {

    private ArrayList<Customer> customer;

    private static int counter = 1;
    public CustomerDirectory() {
        this.customer = new ArrayList<Customer>();
    }
    
    public int setcust(Customer c) {
        c.setID(counter);
        customer.add(c);
        counter++;
        return c.getID();        
    }

    public ArrayList<Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(ArrayList<Customer> customer) {
        this.customer = customer;
    }
}
