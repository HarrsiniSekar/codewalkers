/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO

    // find 5 comments which have the most likes
    // TODO
    public void getTopFiveLikedComments() {

        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        Comparator<Comment> test = new Comparator<Comment>() {
            public int compare(Comment c1, Comment c2) {
                return c2.getLikes() - c1.getLikes();
            }
        };
        Collections.sort(commentList, test);
        System.out.println("sample qn \n");

        for (int i = 0; i < commentList.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }

    }

    public void getTopFiveInactiveCommentUsers() {
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<User> u = new ArrayList<User>(users.values());
        Comparator<User> test = new Comparator<User>() {
            public int compare(User u1, User u2) {
                return Integer.compare(u1.getComments().size(), u2.getComments().size());
            }
        };
        System.out.println("\n Question - 5 Top Five Inactive Users based on Total Comments they created \n");

        Collections.sort(u, test);
        for (int i = 0; i < u.size() && i < 5; i++) {
            System.out.println(u.get(i));
        }
    }

    public void getTopFiveOverallInactiveUsers(String order) {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> u = new ArrayList<Post>(posts.values());
        List<Comment> comment = new ArrayList<Comment>(comments.values());
        HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<User> user = new ArrayList<User>(users.values());
        for (Comment comm : comment) {
            if (temp.containsKey(comm.getUserId())) {
                temp.put(comm.getUserId(), temp.get(comm.getUserId()) + 1 + comm.getLikes());
            } else {
                temp.put(comm.getUserId(), 1 + comm.getLikes());
            }
        }
        for (Post p : u) {
            if (temp.containsKey(p.getUserId())) {
                temp.put(p.getUserId(), temp.get(p.getUserId()) + 1);
            } else {
                temp.put(p.getUserId(), 1);
            }
        }

        LinkedHashMap<Integer, Integer> mapped = sortHashMapByValues(temp, order);
        int count = 0;
        for (int i : mapped.keySet()) {
            if (count < 5) {
                int id = user.get(i).getId();
                String firstName = user.get(i).getFirstName();
                String lastName = user.get(i).getLastName();
                System.out.println("User{id = " + id + ", firstName = " + firstName + ", LastName = " + lastName + ", Total Comments, Likes and Posts = " + mapped.get(i) + "}");
                count++;
            }
        }

    }

    public void getAvgLikesperComment() {

        int average;
        int sum = 0;
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        for (Comment c1 : commentList) {

            sum += c1.getLikes();
        }
        average = sum / commentList.size();
        System.out.println("\n Question - 1 \n");
        System.out.println("The average number of likes per comment is :" + average);
    }

    public void getInactiveUsersbasedonPosts() {

        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        Map<Integer, User> user = DataStore.getInstance().getUsers();
        List<User> users = new ArrayList<User>(user.values());
        List<Post> postList = new ArrayList<Post>(post.values());
        HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>();
        //Collections.sort(commentList,test);
        for (Post p : postList) {
            if (temp.containsKey(p.getUserId())) {
                temp.put(p.getUserId(), 1 + temp.get(p.getUserId()));
            } else {
                temp.put(p.getUserId(), 1);
            }
        }
        String order = "DES";

        LinkedHashMap<Integer, Integer> mapped = sortHashMapByValues(temp, order);

        int count = 0;
        for (int i : mapped.keySet()) {

            if (count < 5 && count < mapped.size()) {
                System.out.println("User{userid = " + user.get(i).getId() + "  No.of Post =" + mapped.get(i) + "}");
                count++;
            }
        }

    }
    //Question Number 3

    public void getMostCommentedPost() {
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(post.values());
        Comparator<Post> test = new Comparator<Post>() {
            public int compare(Post p1, Post p2) {
                return Integer.compare(p2.getComments().size(), p1.getComments().size());
            }
        };
        Collections.sort(postList, test);
        for (int i = 0; i < postList.size() && i < 1; i++) {
            System.out.println("\n Question No 3 - Find the post with most comments \n");
            System.out.println(postList.get(i));

        }

    }   //Question Number 2

    public void getMostLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        Comparator<Comment> test = new Comparator<Comment>() {
            public int compare(Comment c1, Comment c2) {
                return c2.getLikes() - c1.getLikes();
            }
        };
        Collections.sort(commentList, test);

        for (int i = 0; i < commentList.size() && i < 1; i++) {
            System.out.println("\n Question No 2 - Find the post with most liked comments: \n");
            System.out.println(commentList.get(i));

        }
    }

    public LinkedHashMap<Integer, Integer> sortHashMapByValues(
            HashMap<Integer, Integer> passedMap, String order) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        if (order.equalsIgnoreCase("ASC")) {
            Collections.sort(mapValues);
            Collections.sort(mapKeys);
            System.out.println("\n Question - 6  Top Five Inactive Users based on Comments, Posts, Likes \n");
        } else if (order.equalsIgnoreCase("DES")) {
            Collections.sort(mapValues);
            Collections.sort(mapKeys);
            System.out.println("\n Question - 4  Top Five Inactive Users based on Posts \n");

        } else {
            Collections.sort(mapValues, Collections.reverseOrder());
            Collections.sort(mapKeys, Collections.reverseOrder());
            System.out.println("\n Question - 7 Top Five Proactive users based on comments, posts, likes \n");

        }

        LinkedHashMap<Integer, Integer> sortedMap
                = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }

}
